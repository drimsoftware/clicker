Test case for Back-end Developer  
http://www.tapgerine.com/  
  
Скриншоты работающего приложения можно увидеть в директории "screenshots".  
Для ускорения разработки приложение написано без тестов. Есть только стандартные тесты с codeception.  
Запустить их можно так: codecept run  
Вместо автоинкремента для кликов используется UUID (стандартная функция в MySql, хотя можно было бы генерировать ее и на стороне PHP).  
  
---  
  
Used:  
PHP 7.0  
Framework: Yii2 (http://www.yiiframework.com/)  
DB: MySql  
  
Instead autoincrement used UUID (https://ru.wikipedia.org/wiki/UUID)