<?php

namespace app\modules\click\controllers;

use Yii;
use yii\web\Controller;
use app\models\Click;
use app\models\BadDomains;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `click` module
 */
class DefaultController extends Controller
{
    public function actionClick($param1, $param2)
    {
        $utils = Yii::$app->utils;
        $model = Click::clickModel($utils->userAgent, $utils->ip, $utils->referrer, $param1);
        $status = ''; // success | error
        
        if ($model->isNewRecord) {
            $model->ua = $utils->userAgent;
            $model->ip = $utils->ip;
            $model->ref = $utils->referrer;
            $model->param1 = $param1;
            $model->param2 = $param2;
            $status = 'success';
        } else {
            $model->error += 1;
            $status = 'error';
        }

        if (BadDomains::getRefByName($model->ref)) {
            $model->error += 1;
            $model->bad_domain = 1;
            $status = 'error';
        }
        
        $model->save(false);

        return $this->redirect([
            '/click/default/' . $status,
            'id' => $model->id,
        ]);
    }
    
    /**
     * @param $id
     */
    public function actionSuccess($id)
    {
        $model = $this->findModel($id);
        return $this->render('success', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     */
    public function actionError($id)
    {
        $model = $this->findModel($id);
        return $this->render('error', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Click::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
