<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model app\models\Click
 */

$this->title = 'Click Error';
$this->params['breadcrumbs'][] = $this->title;

if ($model->bad_domain === 1) { // Только для bad domain делаем редирект
    $this->registerJs('setTimeout(function () { location.href = "http://google.com" }, 5000);', yii\web\View::POS_READY);
}
?>

<div class="click-default-error">
    <h1><?= Html::encode($this->title) . ', unique click id = ' . $model->id ?></h1>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ua',
            'ip',
            'ref',
            'param1',
            'param2',
            'error',
            'bad_domain',
        ],
    ]) ?>
</div>
