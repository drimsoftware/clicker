<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model app\models\Click
 */

$this->title = 'Click Success';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="click-default-success">
    <h1><?= Html::encode($this->title) . ', unique click id = ' . $model->id ?></h1>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ua',
            'ip',
            'ref',
            'param1',
            'param2',
            'error',
            'bad_domain',
        ],
    ]) ?>
</div>
