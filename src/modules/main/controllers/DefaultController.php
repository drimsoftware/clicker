<?php

namespace app\modules\main\controllers;

use Yii;
use yii\web\Controller;
use app\models\Click;
use app\models\ClickSearch;

/**
 * Default controller for the `main` module
 */
class DefaultController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /**
     * Home page
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ClickSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Links page
     * @return string
     */
    public function actionLinks()
    {
        $param1 = uniqid();
        $param2 = uniqid();
        return $this->render('links', [
            'param1' => $param1,
            'param2' => $param2,
        ]);
    }
}
