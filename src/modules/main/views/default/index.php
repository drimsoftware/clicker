<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Collapse;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClickSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home';
?>
<div class="main-default-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo Collapse::widget([
        'items' => [
            [
                'label' => 'Поиск по кликам. Дабы не грузить лишние библиотеки пользуемся стандартными средствами Bootstrap и Yii2',
                'content' => $this->render('_search', ['model' => $searchModel]),
            ],
        ]
    ]); ?>
    <h4>Чтобы сохранить новый клик перейдите по <?= Html::a('ссылке', ['/main/default/links']); ?></h4>
    <div class="body-content">
        <div class="row">
            <div class="col-md-12">
                <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            'id',
                            'ua',
                            'ip',
                            'ref',
                            'param1',
                            'param2',
                            'error',
                            'bad_domain',
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
