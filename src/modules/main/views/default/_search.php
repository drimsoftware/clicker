<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClickSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-default-search">

    <h3>Search:</h3>

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ua') ?>

    <?= $form->field($model, 'ip') ?>

    <?= $form->field($model, 'ref') ?>

    <?= $form->field($model, 'param1') ?>

    <?= $form->field($model, 'param2') ?>

    <?php // echo $form->field($model, 'error') ?>

    <?php // echo $form->field($model, 'bad_domain') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
