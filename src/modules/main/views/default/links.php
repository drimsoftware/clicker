<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Referrer links';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-default-links">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="body-content">
        <div class="row">
            <div class="col-md-12">
                <h4>Параметры получены случайно, с помощью uniqid()</h4>
                <p>Param 1: <?= $param1; ?></p>
                <p>Param 2: <?= $param2; ?></p>
                <?=  Html::a('Referrer link', Url::to(['/click/default/click', 'param1' => $param1, 'param2' => $param2]))?>
            </div>
        </div>
    </div>
</div>
