<?php

use yii\db\Migration;

/**
 * Handles the creation for table `bad_domains`.
 */
class m161016_162101_create_bad_domains_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
		$dbType = $this->db->driverName;
		$tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
		$tableOptions_mssql = "";
		$tableOptions_pgsql = "";
		$tableOptions_sqlite = "";
		/* MYSQL */
		if (!in_array('bad_domains', $tables))  { 
			if ($dbType == "mysql") {
				$this->createTable('{{%bad_domains}}', [
					'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
					0 => 'PRIMARY KEY (`id`)',
					'name' => 'VARCHAR(255) NOT NULL',
				], $tableOptions_mysql);
			}
		}
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bad_domains');
    }
}
