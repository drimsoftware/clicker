<?php

use yii\db\Migration;

/**
 * Handles the creation for table `click`.
 */
class m161016_161154_create_click_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tables = Yii::$app->db->schema->getTableNames();
		$dbType = $this->db->driverName;
		$tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
		$tableOptions_mssql = "";
		$tableOptions_pgsql = "";
		$tableOptions_sqlite = "";
		/* MYSQL */
		if (!in_array('click', $tables))  { 
			if ($dbType == "mysql") {
				$this->createTable('{{%click}}', [
					'id' => 'VARCHAR(36) NOT NULL',
					0 => 'PRIMARY KEY (`id`)',
					'ua' => 'VARCHAR(255) NOT NULL',
					'ip' => 'VARCHAR(15) NOT NULL',
					'ref' => 'VARCHAR(255) NOT NULL',
					'param1' => 'VARCHAR(255) NOT NULL',
					'param2' => 'VARCHAR(255) NOT NULL',
                    'error' => 'INT(11) NOT NULL DEFAULT \'0\'',
					'bad_domain' => 'INT(11) NOT NULL DEFAULT \'0\'',
				], $tableOptions_mysql);
			}
		}
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('click');
    }
}
