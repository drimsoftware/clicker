<?php
namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Класс-помощник для получения IP, User-Agent, Referrer
 */
class Utils extends Component
{
    public function init()
    {
        parent::init();
    }

    /**
     * Get IP address
     */
    public static function getIp()
    {
        $ip = !empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    /**
     * Get user agent
     */
    public function getUserAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * Get referrer
     */
    public function getReferrer()
    {
        return (!empty(Yii::$app->request->referrer)) ? Yii::$app->request->referrer : '';
    }
}