<?php
namespace app\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Поведение для поддержки UUID
 * https://ru.wikipedia.org/wiki/UUID
 * Используется стандартная функция MySql: UUID()
 * http://dev.mysql.com/doc/refman/5.7/en/miscellaneous-functions.html#function_uuid
 */
class UUIDBehavior extends Behavior
{
    /**
     * UUID-column
     * Default -> id
     */
    public $column = 'id';
    
    /**
     * Override events() 
     * method beforeSave() from component ActiveRecord::EVENT_BEFORE_INSERT  
     */
    public function events() {
        return[
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
        ];
    }
    
    /**
     * Update UUID-column
     * Used default MySql UUID() function
     */
    public function beforeSave() {
        $this->owner->{$this->column} = $this->owner->getDb()->createCommand("SELECT REPLACE(UUID(),'-','')")->queryScalar();
    }
}