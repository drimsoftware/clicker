<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bad_domains".
 *
 * @property integer $id
 * @property string $name
 */
class BadDomains extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bad_domains';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'url'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getRefByName($name)
    {
        return self::find()
            ->where(['name' => $name])
            ->one();
    }
}
