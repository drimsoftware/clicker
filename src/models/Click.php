<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "click".
 *
 * @property string $id
 * @property string $ua
 * @property string $ip
 * @property string $ref
 * @property string $param1
 * @property string $param2
 * @property integer $error
 * @property integer $bad_domain
 */
class Click extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'click';
    }
    
    public function behaviors()
    {
        return [
            \app\components\UUIDBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['ua', 'ip', 'ref', 'param1', 'param2'], 'required'],
            [['error', 'bad_domain'], 'integer'],
            [['ua', 'ref', 'param1', 'param2'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ua' => 'User Agent',
            'ip' => 'IP',
            'ref' => 'Referrer',
            'param1' => 'Param1',
            'param2' => 'Param2',
            'error' => 'Error',
            'bad_domain' => 'Bad Domain',
        ];
    }
    
    /**
     * @param $ua
     * @param $ip
     * @param $referrer
     * @param $param1
     * @return Click
     */
    public static function clickModel($ua, $ip, $referrer, $param1)
    {
        $model = self::find()
            ->where([
                'ua' => $ua,
                'ip' => $ip,
                'ref' => $referrer,
                'param1' => $param1
            ])
            ->one();

        return (!$model) ? new Click() : $model;
    }
}
