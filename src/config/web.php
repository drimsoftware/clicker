<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'tap_clicker',
    'name' => 'Clicker',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'defaultRoute' => 'main/default/index',
    'modules' => [
        'main' => [
            'class' => 'app\modules\main\MainModule',
        ],
        'click' => [
            'class' => 'app\modules\click\ClickModule',
        ],
        'domains' => [
            'class' => 'app\modules\domains\DomainsModule',
        ],
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => '7E_rc0OJHzX5NFdr7dHcaqhOnmCSKVLP',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'linkAssets' => true,
        ],
        'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            
            'enableStrictParsing' => true,
            'suffix' => '',
            
            'rules' => [
                '/' => '/main/default/index',
				'home' => '/main/default/index',
                'links' => '/main/default/links',
                'site-error' => '/main/default/error',
				
                'domains' => '/domains/default/index',
				'domains/create' => '/domains/default/create',
				'domains/update' => '/domains/default/update',
				'domains/view' => '/domains/default/view',
				'domains/delete' => '/domains/default/delete',
                
                'click' => '/click/default/click',
                [
			        'pattern' => 'success/<id:[\w -.]+>',
			        'route' => '/click/default/success',
					'defaults' => ['id' => 0],
			    ],
                [
			        'pattern' => 'error/<id:[\w -.]+>',
			        'route' => '/click/default/error',
					'defaults' => ['id' => 0],
			    ],
            ],
        ],
        'utils' => [
            'class' => 'app\components\Utils',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
